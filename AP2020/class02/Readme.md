## Class 02: Variable Geometry 

**Messy notes:**

**Agenda:**
1. Mini-ex[1] discussion + why feedback 
2. More on p5.js - more than just a tool 
3. Mini-lecture
4. Project showcase: Multi by David Reinfurt
5. Sample Code walkthrough + functions/syntax + Coordination + Variables + Operators + Conditional Structure
6. Exercise in class
7. Mini-ex walk-through: Geometric emoji | due SUN mid-night

---

### 1. Mini-ex[1] discussion + why feedback 

- what are the tools (atom+p5.js+gitlab) again
- Files naming and folder structure
- The concept of README and RUNME again 
- How you might work on your README? (the linkage with the reading is IMPORTANT!)
- Why we need [feedback](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2020/ex) and what's feedback? 
- What's writing vs reading code? 
- 1x student will present the miniEx[1] 
- 2x students will talk about the feedback to the class 

---

### 2. More on p5.js - more than just a tool 
- open source culture
- software is not just a tool but also about people and politics 
- show Lauren's video (10 mins) 

---

### 3. Mini-lecture
- The notion of [Fun](https://www.bloomsbury.com/uk/fun-and-software-9781623568870/) 
- Fun with Geometry 
- [Pervasive emojis](http://publish.illinois.edu/iaslibrary/files/2015/10/MAC-Emoji.png) -> expressing emotional states 
- Politics of representation -> not just for fun -> Modifying the Universal
- Unicode as a computing industry standard (see: https://unicode.org/emoji/charts/full-emoji-list.html)
- Oversimplfication and Universalise differences

**Discussion:**

Examine [existing emojis](https://printable360.com/wp-content/uploads/2018/01/printable-pictures-of-emojis-f85e23c6c5560b017f1154346490d23d.jpg) or those available on your handset, and reflect upon the complexity of human emotions and their caricature.  What's your experience in using emojis? What are the cultural and political implications of emojis (perhaps yopu can refer to the readings)?

---

### 4. Project showcase: Multi by David Reinfurt

<img src="https://rag532wr4du1nlsxu2nehjbv-wpengine.netdna-ssl.com/wp-content/uploads/2014/09/Reinfurt_BW-600x0-c-default.jpg" width ="300">

- independent graphic designer and writer in New York City
- more: https://arts.princeton.edu/people/profiles/reinfurt/

What's <a href="http://www.o-r-g.com/apps/multi">Multi</a>:

Multi is inspired by another designer Enzo Mari who spent a whole year in 1957 exploring the **essential form** of an object (an apple, the universal apple we might add). Reinfurt explains that "He was not looking to draw AN apple, but rather THE apple — a perfect symbol designed for the serial logic of industrial reproduction." Multi develops a variation of this idea for informational reproduction in the form of a mobile app with 1,728 possible arrangements, or facial compositions, built from minimal punctuation glyphs. 

<img src="http://www.o-r-g.com/media/00031.png" width="300"> <br>
reference: La Mela (The Apple), drew by Italian's designer Enzo Mari (1932)

[link to the history of the Apple logo ](https://www.fineprintart.com/history-of-the-apple-logo/)

<img src="http://o-r-g.com/media/00004.gif" height="400"><br>
*Multi by David Reinfurt* 

- part of the <a href="http://www.deliciousindustries.com/blog/enzo-maris-nature-series">nature series</a>
- Multi on a phone: <br> <img src="http://o-r-g.com/media/00028.png" height="500">
- Multi on the web: http://www.data-browser.net/
- Multi on a book: http://www.data-browser.net/db06.html
- Present 1728 possible arrangements
- considered the process of "the serial logic of industrial reproduction" and reflect upon contemporary software production

Cartoon characters to think through how vector graphical tools work: 

Miffy (1955) by Dutch artist Dick Bruna. <br>
<img src="http://www.iconofgraphics.com/bruna/large/dick_bruna_nijntje_miffy.jpg"> 

The Moomins by Swedish-speaking Finnish illustrator Tove Jansson <br>
<img src = "http://images6.fanpop.com/image/photos/37400000/yay-moomin-the-moomins-37430202-862-600.jpg" width="450">

---

### 5. Sample Code walkthrough

The sample code draws various shapes and performs simple interactions: http://siusoon.gitlab.io/aesthetic-programming/ 

PAIR DISCUSSION: 

1. Decode:
Based on what you see/experience on the screen, describe:
- Describe and **list** out what you see on the screen in details
- What is moving and not moving, what is changing and not changing?
- Without looking at the source code, can you imagine what kind of syntax you might need to code this? 

2. Ex: Mapping the code and find out what's the syntax about via looking at the p5.js [reference guide](https://p5js.org/reference/) 
(working with printed source code)

3. Discussion about the syntax/functions -all: `frameRate()`, `rect()`, `noStroke()`, `fill()`, beingShape + endShape + Vertex(), `strokeWeight()`, `noFill();`, `floor(random());`, `mouseIsPressed()`

### 5.1. Coordination + Variables 

**Coordination:** is a fundamental concept for positioning and drawing objects with various measurements on a canvas. 
- `createCanvas(windowWidth,windowHeight)` vs `createCanvas(640,480);`

**Variables** are used to store data and information in a computer program. You can think of variables as a kitchen container, and you can put things (like food, forks, etc) in a given container, replace them with other things, and store them for later retrieval.

- go to web console and type: `print(width);` then press enter 
- go to web console and type: `console.log(width, height);` then press enter 

**Assigning your own variables**: 
```javascript
let moving_size = 50;
let static_size = 20;
.
.
.
ellipse(255,350,static_size,static_size);
.
.
.
ellipse(mouseX, mouseY, moving_size, moving_size);

if (mouseIsPressed) {
    static_size = floor(random(5, 20));
}
```

1. **Declare:** Think of a *name* for the container you want to store the value in (usually it will make sense to yourself and to others to read it, but of course there is scope for a conceptual approach here). Declare with the syntax 'let' in front.
2. **Assign:** What is it that you want to store there? Is it a number? By assigning a value, you will need to use the equal sign. Officially, there are 4 data types that are useful to know at this introductary level:
    1. number for numbers of any kind: integer or floating-point.
    2. string for strings. A string may have one or more characters and it has to be used with double or single quote marks. For example: `let moving_size = "sixty";`
    3. boolean for true/false. For example: `let moving_size = true;`
    4. color for color values. It can take in Red Green, Blue (RGB) or Hue, Saturation and Brightness (HSB) values. For example: `let moving_size = color(255,255,0);` (see more from the [p5.js color reference](https://p5js.org/reference/#/p5/color))
3. **(Re)Use:** How and when do you want to retrieve the stored data? If the variable will change over time, you may want to reuse it many times.

**Local variable vs Global variable vs Constant**

**Discussion and Ex:**
Take a look at how mouseX and mouseY are being used, and if you want to know the coordinate of these two when you press your mouse, how to do it? (hint: use the print or console.log function and to consider mouseX and mouseY are changing variables)

**Variable - memory - addresses - hardware**

<img src="https://2r4s9p1yi1fa2jd7j43zph8r-wpengine.netdna-ssl.com/files/2017/06/01_06.png" width="400"> 

*img ref: http://brianyang.com/a-crash-course-in-memory-management/* 

**Why variables are important to know and good to use?** 

---

### 5.2 Operators 

You can also do arithmetic operations in programming, and this is commonly done in arguments of a function. 
Here is a list of basic arithmetic operators:
- add(+): For addition and concatenation, which is applicable for both numbers and text/characters.
- subtract(-)
- multiply(*)  
- divide(/)
- Special operators: increment (++), decrement (--)   

You can try the following in the console area:
```
print(2*3);
```
> Output:
"6"
```
print("hello" + "world");
```
> output:
"hello world"

---

### 5.3 Conditional Structure

*pseudocode* to demonstrate what making an everyday decision to eat or drink might look like in programming. 

```javascript
//example in human language
if (I am hungry) { 
  eat some food;
} else if(thirsty) {
  drink some water;
} else{
  take a nap;
}
```
The whole if statement is a *Boolean expression* — one of two possible values is possible, true or false, leading to a different path and action. 

In our sample code, we have implemented a conditional logic to constantly check if there is any mousepressed actions. This is why the size of the ellipse changes when a mouse is pressed. 

```javascript
  if (mouseIsPressed) {
    static_size = floor(random(5, 20));
 }
```
---

### 5.4 Relational operators 

When you have to create your own conditional statement with the if-then format, there are multiple combinations you can work on to form a more complex expression. For example, you can have many different cases using the syntax `else if`, or a combinaton of logical operators, such as the AND case here in another pseudocode example: 

```javascript 
if (I am hungry) && (I am in a good mood) {
    print("go out");
}
```

Here is a list of relational operators and logical symbols that can be used in conditional statements. 

```
/* 
Relational Operators: 
>   Greater than
<   Less than
>=  greater than or equal to
<=  less than or equal to
==  equality
=== equality (include strict data type checking)
!=  not equal to
!== inequality with strict type checking
*/

/*
Logical Operators: boolean logic:
&&  logical AND
||  logical OR
!   logical NOT
*/

/*
Example: 
if () {
  //something here
}else if() {
  //something here
}else{
  //something here
}
*/
```

Question for discussion: 
https://play.kahoot.it/v2/intro?quizId=b7bff7cc-b645-4276-8613-3fe91080603c

Go to: www.kahoot.it


```javascript
let x = 18;
if (x > 10 || x <= 20 ) {
    console.log("one");
}else if ( x == 18) {
    console.log("two");
}else if (x === 18) {
    console.log("three");
}else  {
    console.log("four");
}
```

---
### 6.Exercise in class

1. Discuss what constitutes a face? What essential elements do you need for a particular facial expression, and why? What has been lost in translation?
2. Beyond the face, take a look at [more emojis](https://www.pngfind.com/mpng/ohwmTJ_all-the-emojis-available-on-facebook-russian-revolution/). Is there anything you want to add? 
3. Explore the interactive online tool [p5.playground](https://1023.io/p5-inspector/) developed by Yining Shi, and do some sketching of emojis by paying attention to spatial composition and foundational design elements. 
4. Experiment with p5.js. How do you translate your thoughts into lines of code? You may want to print out the coordinates of the mouse press on the console area to get a more accurate position for your shapes. 

---

### 7. Mini-ex walk-through: Geometric emoji | due SUN mid-night

Walkthrough [2]: Geometric emoji + feedback arrangement 

https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2020/ex

### 7.1 Next class presentation - max 20 mins

Group 1: 
transform - scale/rotate/translate/push/pop with the text: Femke Snelting, "Other Geometries", transmediale journal, Issue#3, 31 October 2019, https://transmediale.de/content/other-geometries)

*with your own sample code/experimentation - address both the technical understanding and the conceptual thinking with the text*