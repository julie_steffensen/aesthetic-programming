Here is the protocol for the weekly RUNME (a program that can be executed) and README (contextualize your thoughts on the program) mini exercises:

## Weekly mini-Exercise (Both RUNME and README)
- **Due on every SUN mid night** (those who are late submission might miss the chance to receive feedback from your peers and fail to enter the ordinary exam)
- Upload 1x README and 1x RUNME on your gitlab account
- Suggest to have the folder miniEx in your gitlab repository and create a folder miniEx to store all your README and RUNME.
- See the README's markdown guide: https://about.gitlab.com/handbook/engineering/technical-writing/markdown-guide/
- The README.md (within 5600 characters): 
    - 1 x screenshot 
    - 1 x URL that links to your work 
    - 1 x URL that links to your repository (at the code level)
    - Answer/approach the questions stated in the weekly brief (Try your best to reflect on/link to the assigned readings)
    - Reference URLs (if any - this is more to see where you get your inspiration and ideas)
- The RUNME (the software program):
    - js libraries
    - a HTML file
    - a js sketch file 
- Everyone need to prepare to be asked showing and articulating your project in the class every week

## Weekly feedback 
- **Due date: before next class**
- Your group will be splitted into 2. (In general, 2x group feedback is needed for each mini group) For example:
   - class 2: 2 people in group 1 feedback 2 of the students in group 2. 
   - class 3: 2 people in group 1 feedback 2 of the students in group 3. 
   - class 4: 2 people in group 1 feedback 2 of the students in group 4. 
   - ...
- How: Go to "issues" on his/her gitlab corresponding repository. Write the issue with the title "**Feedback on mini_ex(?) by (2x YOUR FULL NAME)**"

## How to do feedback
(around 2000 characters)

For the MiniX[1]:
- Describe what is the program about and what syntax were used
- Reflect upon what are the differences between reading other people code and writing your own code. What can you learn from reading other works?

For the miniX[2]:
- (Just think, no need to write) Think about what kind of feedback you think would be useful to others. What kind of feedback you want to receive by yourself?
- Describe what is the program about, what syntax was used, what does the work express?
- Do you like the design of the program, and why? and which aspect do you like the most? How would you interprete the work?
- How about the conceptual linkage about the work beyond technical description and implementation? What's your thoughts on this?

## Mini_Exercise[1]: My First Program

See the section Mini_Exercise[1]: My First Program:

https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/tree/master/source/1-GettingStarted

## Mini_Exercise[2]: Geometric Emoji 

See the section Mini_Exercise[2]:

https://gitlab.com/siusoon/Aesthetic_Programming_Book/blob/master/source/2-VariableGeometry/Readme.md

## Final Project

The final project will be announced on class 8 and the deadline for that would be on 12 May via the submission on digitalexam as well as presenting your group project in-class.