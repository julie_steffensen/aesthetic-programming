# Aesthetic Programming 2020 @ Aarhus University

**Course title:** Aesthetic Programming (20 ECTS), 2020 <br>
**Name:** Winnie Soon (wsoon@cc.au.dk)<br>
**Time:** Every Tue 0900 – 1300 <br>
**Location:** 5361-144, unless otherwise stated

!NB:
* Tutorial: Every Wed 14.00-16.00 @ 5524-147 or 5524-139, conducted by Ann Karring and Noah Sanchez Echevarria Aamund
* Shutup and code: Wk 6-10, Friday 10.00-14.00 @ 5361 - 144, conducted by Ann Karring/Noah Sanchez Echevarria Aamund
* class detail: Groups in https://pad.riseup.net/p/AP2020-keep (expired in a year)
* discussion forum: https://padlet.com/siusoon/iaaabkoo9qey
* sample code repository: http://siusoon.gitlab.io/aesthetic-programming/
* Weekly mini-exercises + feedback: https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2020/ex

## OUTLINE:
“[Aesthetic Programming](https://gitlab.com/siusoon/Aesthetic_Programming_Book/tree/master/source/0-Preface)” is a practice-oriented course requires no prior programming experience, exploring the relationship between code and cultural and/or aesthetic issues related to the research field of software studies. The purpose of the course is to enable the students to design and programme a piece of software, to understand programming as an aesthetic and critical action which extends beyond the practical function of the code, and to analyse, discuss and assess software code based on this understanding. The course has links to Software Studies on the same semester, and the purpose of the collaboration between these two courses is to provide specific experiences with programming as a reflected and critical practice.
   
In the course, programming practices are considered as a way to describe and conceptualise the surrounding world in formal expressions, and thus also as a way to understand structures and systems in contemporary digital culture. We will use [P5.js](https://p5js.org/) primarily, which serves as a foundation for further courses on Digital Design. 

## TASKS and Expectation (prerequisite for ordinary exam - Oral exam)
1. Read all the required reading and watch all the required instructional video before coming to the class.
2. Active participation in class/instructor discussion and exercises (including the attendance to PCD @ Aarhus on 22 Feb and the possibble guest lecture)
3. Do and submit all the WEEKLY mini exercises (mostly individual with a few group works) on time 
4. Provide WEEKLY peer feedback online and on time
5. Peer-tutoring in a group and with a given topic: within 20 mins in-class presentation 
6. Submission of the final group project - in the form of a “readme” and a “runme” (software) package

!NB: 20 ECTS is equivalent to around 25 hours per week, including lecture and tutorial. 

!NB: Oral exam (9-11 Jun 2020)

## Other learning support environment:
1. Weekly 2 hours tutorial session (every Wed 14.00-16.00)
2. Weekly 4 hours shut up+code or code study group discussion (every Friday 10.00-14.00 from week 6-10
3. Digital Design Lab 
4. Aesthetic Programming / Software Studies bookshelf in AU library (Nygaard building)

![library](library.jpg)

## LEARNING OUTCOMES:
1. Read and write computer code in a given programming language
2. Use programming to explore digital culture
3. Understand programming as an aesthetic and critical action which extends beyond the practical function of the code

## CLASS SCHEDULE:
#### Class 01, 0900-13.00 | Week 6 |4 Feb: Getting Started
##### With Wed tutorial session and Fri shutup and code session.

**Required Reading**
* Nick Montfort, "Appendix A: Why Program?", Exploratory Programming For the Arts and Humanities (Cambridge, Mass.: MIT Press, 2016), 267-277. (on blackbord under the Literature folder)
* Video: Lauren McCarthy, [Learning While making P5.JS](https://www.youtube.com/watch?v=1k3X4DLDHdc), OPENVIS Conference (2015).
* Video: Daniel Shiffman, Code! Programming with p5.js, The Coding Train. Available at https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2 [Accessed 09 Sep. 2019]. (**watch 1.1**)
* p5.js. p5.js | get started. Available at: https://p5js.org/get-started/ [Accessed 09 Sept 2019].
* Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93. (on blackbord under the Literature folder)

**Suggested Reading**
* Brian Lennon, "[JavaScript Affogato: Programming a Culture of Improvised Expertise](https://muse.jhu.edu/article/685007/pdf)", *Configurations*, Volume 26, Number 1, Winter 2018, 47-72.

**Prepare this before class:**
  - Why do you think you need to know programming? Why is it important to know programming in the Digital Design programme?

**In-class structure:**
  - Why we need to learn programming?
  - Syllabus, class and learning, expectation
  - What is JavaScript and why p5.js?
  - Setting up the working environment (p5.js lib + Atom code editor) 
  - Run the first p5.js program 
  - In class exercise 
  - Web console + reading reference guide 
  - Git 
  - Mini-ex [1] walk-through: [My First Program](https://gitlab.com/siusoon/aesthetic-programming/tree/master/AP2020/ex) | due SUN mid-night
  - Note: **[PCD @ Aarhus](https://www.pcdaarhus.net/)** on 22 Feb (SAT) - Mandatory
---

#### Class 02, 0900-13.00 | Week 7 | 11 Feb: Variable Geometry
##### With Wed tutorial session and Fri shutup and code session.

**Required Reading**
* Video/text: Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." [Executing Practices](http://www.openhumanitiespress.org/books/titles/executing-practices/), Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. or Femke Snelting, [Modifying the Universal](https://www.youtube.com/watch?v=ZP2bQ_4Q7DY), MedeaTV, 2016 [Video, 1 hr 15 mins].
* p5.js. p5.js | Simple Shapes. [Web] Available at: https://p5js.org/examples/hello-p5-simple-shapes.html [Accessed 09 Sep. 2019].
* Daniel Shiffman, Code! Programming with p5.js, The Coding Train [online] Available at: https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2 [Accessed 09 Sep. 2019]. [**watch 1.3,1.4,2.1,2.2**]
* short text: Femke Snelting, "Other Geometries", transmediale journal, Issue#3, 31 October 2019, https://transmediale.de/content/other-geometries

**Suggested Reading**
* Crystal Abidin and Joel Gn, eds., "Histories and Cultures of Emoji Vernaculars", First Monday, 23(9), September, 2018, https://firstmonday.org/ojs/index.php/fm/issue/view/607
* Christian Ulrik Andersen and Geoff Cox, eds., A Peer-reviewed Journal About Machine Feeling, 8(1), 2019, https://tidsskrift.dk/APRJA/issue/view/8133.
* Olga Goriunova, Olga, "Introduction", Fun and Software: Exploring Pleasure, Paradox and Pain in Computing, New York, London: Bloomsbury, 2014, 1-19.
* Derek Robinson, "Variables", in Matthew Fuller, ed., Software Studies, Cambridge, Mass.: MIT Press, 2008.

**In-class structure:**
  - Project showcase: *Multi* by David Reinfurt
  - Coordination + Variables + Functions + Operators + Conditional Structure
  - Exercise in class
  - Mini-ex walk-through: Geometric emoji | due SUN mid-night
  - Note: **[PCD @ Aarhus](https://www.pcdaarhus.net/)** on 22 Feb (SAT) - Mandatory
---

#### Class 03, 0900-13.00 | Week 8 | 18 Feb: Infinite loops  
##### With Wed tutorial session and Fri shutup and code session.

**Required Reading**
* Daniel Shiffman, [Code! Programming with p5.js on YouTube](https://www.youtube.com/watch?v=1Osb_iGDdjk), The Coding Train. (practical usage on conditional statements, loops, functions and arrays - **watch 3.1, 3.2, 3.3, 3.4, 4.1, 4.2, 5.1, 5.2, 5.3, 7.1, 7.2**)
* Soon, Winnie. "[Throbber: Executing Micro-temporal Streams](http://computationalculture.net/throbber-executing-micro-temporal-streams/)", Computational Culture, 2019.
* Close reading on the work [Asterisk Painting](https://editor.p5js.org/siusoon/sketches/YAk1ZCieC) by John P. Bell, ported to p5.js, and modified, by Winnie Soon.

**Suggested Reading**
* Wilfried Hou Je Bek, "Loop", *Software Studies* (Fuller, Matthew, ed.), MIT Press, 2008.
* Robinson, Derek. "Function", *Software Studies* (Fuller, Matthew, ed.), MIT Press, 2008.
* Farman, Jason. "[Fidget Spinners](https://reallifemag.com/fidget-spinners/#!)", *Real Life*, 2017.
* Wolfgang Ernst, “‘... Else Loop Forever’. The Untimeliness of Media” (2009). Available at [https://www.medienwissenschaft.hu-berlin.de/de/medienwissenschaft/medientheorien/downloads/publikationen/ernst-else-loop-forever.pdf](https://www.medienwissenschaft.hu-berlin.de/de/medienwissenschaft/medientheorien/downloads/publikationen/ernst-else-loop-forever.pdf).

**In-class structure:**
  - Group 1 presentation: Transform...
  - Decode
  - Function + Arrays + Relational Operators
  - Exercise in class
  - Asterisk Painting
  - Loops and While Loop
  - Mini-ex walk-through: Designing a Throbber | due SUN mid-night
  - Note: **[PCD @ Aarhus](https://www.pcdaarhus.net/)** on 22 Feb (SAT) - Mandatory
---

#### Class 04, 11.30-14.00 @ DOKK1 | Week 8 | 22 Feb: [PCD @ Aarhus ](https://www.pcdaarhus.net/) 

with workshops (need to sign up) and  5x speakers 

---
#### Class 05, 0900-13.00 | Week 9 | 25 Feb: Data Capture  
##### With Wed tutorial session and Fri shutup and code session.

**Required Reading**
* Carolin Gerlitz and Anne Helmond, "The Like Economy: Social Buttons and the Data-Intensive Web", New Media & Society 15: 8 (December 1, 2013): 1348–65.
* Søren Pold, "Button", in Matthew Fuller, ed. Software Studies (Cambridge, Mass.: MIT Press 2008).
* [p5.js examples - Interactivity 1](https://p5js.org/examples/hello-p5-interactivity-1.html).
* [p5.js examples - Interactivity 2.](https://p5js.org/examples/hello-p5-interactivity-2.html)
* [p5 dom reference](https://p5js.org/reference/#group-DOM).

**Suggested Reading**
* Christian Ulrik Andersen and Geoff Cox, eds. ,[ A Peer-Reviewed Journal About Datafied Research](https://aprja.net/issue/view/8402), APRJA 4.1 (2015).
* Rena Bivens, "The Gender Binary Will Not be Deprogrammed: Ten Years of Coding Gender on Facebook", New Media & Society 19(6) (2017): 880–898, https://doi.org/10.1177/1461444815621527.
* Kenneth Cukier and Victor Mayer-Schöenberger, "The Rise of Big Data", Foreign Affairs (May/June 2013): 28–40.
* clmtrackr - [Face tracking javascript library](https://github.com/auduno/clmtrackr) by Audun M. Øygard.
* Daniel Shiffman, 8. HTML / CSS/DOM - p5.js Tutorial, The Coding Train. Available at: https://www.youtube.com/playlist?list=PLRqwX-V7Uu6bI1SlcCRfLH79HZrFAtBvX [Accessed 09 Sep. 2019].
* Tiziana Terranova, "Red Stack Attack! Algorithms, Capital and the Automation of the Common", EuroNomade, 2014. Available at http://www.euronomade.info/?p=2268

**In-class structure:**
  - Group 2 presentation: DOM reference ...
  - Exercise in class
  - Creating and styling a button
  - Mouse + Keyboard + Audio + Face Capture 
  - The concept of capture
  - Mini-ex walk-through:Capture All | due SUN mid-night
---

#### Class 06, 0900-13.00 | Week 10 | 3 Mar: Pause Week
##### With Wed tutorial session and No Fri shutup and code session.

**Required Reading**
* Soon, W & Cox, G. (2020 forthcoming). Preface in Aesthetic Programming: A handbook of Software Studies. https://gitlab.com/siusoon/Aesthetic_Programming_Book/tree/master/source/0-Preface

**In-class structure:**
  - Group 3 presentation: image/video/audio glitches...
  - Group 4 presentation: Reflection on why we need to code...
  - Discussion on miniEx 
  - Basic Concepts revisit 
  - Mini-ex walk-through: Revisit the past | due SUN mid-night
---

#### Class 07, 0900-13.00 | Week 11 | 10 Mar: Object Abstraction
##### With Wed tutorial session and Fri shutup and code session.

**Required Reading**
* Matthew Fuller & Andrew Goffey, "The Obscure Objects of Object Orientation", in Matthew Fuller, ed. How to be a Geek: Essays on the Culture of Software (Cambridge: Polity, 2017).
* [p5.js examples - Objects](https://p5js.org/examples/objects-objects.html)
* [p5.js examples - Array of Objects](https://p5js.org/examples/objects-array-of-objects.html)
* Daniel Shiffman, [Code! Programming with p5.js on YouTube](https://www.youtube.com/watch?v=8j0UDiN7my4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA), The Coding Train. (**watch: 2.3, 6.1, 6.2, 6.3, 7.1, 7.2, 7.3**)

**Suggested Reading**
* Lee, Roger Y. Software Engineering: A Hands-On Approach. Springer, 2013. 17-24, 35-37 (ch. 2 Object-Oriented concepts)
* Crutzen, Cecile and Kotkamp Erna. Object Orientation. Software Studies\a lexicon. Eds Matthew F. MIT Press, 2008. 200-207
* Black, Andrew P. [Object-oriented Programming: some history, and challenges for the next fifty years](https://arxiv.org/abs/1303.0427). 2013.
* Dahl OJ, "[The Birth of Object Orientation: the Simula Languages](https://link.springer.com/chapter/10.1007/978-3-540-39993-3_3)", in Owe O., Krogdahl S., Lyche T, eds. * Object-Orientation to Formal Methods* Lecture Notes in Computer Science, vol 2635. (Berlin, Heidelberg: Springer, 2004.
* [16.17 Inheritance in JavaScript - Topics of JavaScript/ES6](https://www.youtube.com/watch?v=MfxBfRD0FVU&feature=youtu.be&fbclid=IwAR14JwOuRnCXYUIKV7DxML3ORwPIttOPPKhqTCKehbq4EcxbtdZDXJDr4b0) by Daniel Shiffman

**In-class structure:**
  - Group 5 presentation: 3D objects...
  - Pseudo object
  - Project showcase: *Tofu Go!* by Francis Lam
  - Object Oriented Programming 
  - Exercise in class
  - Mini-ex walk-through: Games with objects | due SUN mid-night
  - Mid-term evaluation
---

#### Class 08, 0900-13.00 | Week 12 | 17 Mar: Auto Generator
##### With Wed tutorial session only (set up your own study group on Fri)

**Required Reading**
* Montfort, N, et al. "Randomness". [10 PRINT CHR$(205.5+RND(1)); : GOTO 10](https://10print.org/), The MIT Press, 2012, pp. 119-146 (The chapter: Randomness)
* [6 mins video] [Langton's Ant Colonies](https://www.youtube.com/watch?v=w6XQQhCgq5c)
* Daniel Shiffman, [noise() vs random() - Perlin Noise and p5.js Tutorial](https://www.youtube.com/watch?v=YcdldZ1E9gU), The Coding Train.
* Daniel Shiffman, [p5.js - 2D Arrays in Javascript](https://www.youtube.com/watch?v=OTNpiLUSiB4), The Coding Train.

**Suggested Reading**
* Langton, Chris G. (1986). "[Studying artificial life with cellular automata](https://deepblue.lib.umich.edu/bitstream/2027.42/26022/1/0000093.pdf)". Physica D: Nonlinear Phenomena. 22 (1–3): 120–149.
* [The Game of Life (1970)](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) by John Horton Conway
* [The Recode Project](http://recodeproject.com/) and [Memory Slam](http://nickm.com/memslam/) by Nick Montfort
* Galanter, Philip. "[Generative Art Theory](http://cmuems.com/2016/60212/resources/galanter_generative.pdf)". A Companion to Digital Art. Eds. Christiane P, 2016.
*[How to Draw with Code](https://www.youtube.com/watch?v=_8DMEHxOLQE) by Casey Reas
* Daniel Shiffman, [p5.js Coding Challenge #14: Fractal Trees - Recursive](https://www.youtube.com/watch?v=0jjeOYMjmDU), The Coding Train.
* Daniel Shiffman, , [p5.js Coding Challenge #76: Recursion](https://www.youtube.com/watch?v=jPsZwrV9ld0), The Coding Train.

**In-class structure:**
  - Group 6 presentation: Recursion/Fractal + Noise()...
  - Turing machine and rule-based art
  - 10 PRINT in class ex 
  - Randomness
  - Langton's Ant 
  - 2D arrays and nested for-loops
  - Exercise in class
  - Mini-ex walk-through: A generative program (optional group work) | due SUN mid-night
  - Walk through Final Project
---

#### Class 09, 0900-13.00 | Week 13 | 24 Mar: Vocable Code
##### With Wed tutorial session only (set up your own study group on Fri)

**BRING a magazine + a newspaper, a scissor and a plastic bag to the class (per group)**

**Required reading:**
* Geoff Cox, and Alex McLean. *Speaking Code*. Cambridge, Mass.: MIT Press, 2013. 17-38.
* Geoff Cox, Alex McLean, and Adrian Ward. "[The Aesthetics of Generative Code](http://www.generativeart.com/on/cic/2000/ADEWARD.HTM)." Proc. of Generative Art. 2001.
* Daniel Shiffman, 10.2: What is JSON? Part I - p5.js Tutorial [online], The Coding Train, Available at: https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r [Accessed 13 Mar. 2019].
* Daniel Shiffman, 10.2: What is JSON? Part II - p5.js Tutorial, The Coding Train, [online] Available at: https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r [Accessed 13 Mar. 2019].

**Suggested Reading:** 
* Parrish, Allison. (2018) Text and Type. [online] Available at: https://creative-coding.decontextualize.com/text-and-type/ [Accessed 13 Mar. 2019].  
* Plant, Sadie. (1998) Zeros + Ones: Digital Women and the New Technoculture. London: Forth Estate.
* Queneau, Ramond, et al. Six Selections by the Oulipo. The New Media Reader. Eds. Noah W-F and Nick M. The MIT Press, 2003. 147-189.
* Raley, Rita. [Interferences: [Net.Writing] and the Practice of Codework](http://electronicbookreview.com/essay/interferences-net-writing-and-the-practice-of-codework/). electronic book review, 2002.
* Rhee, Margaret. “[Reflecting on Robots, Love, and Poetry](https://dl.acm.org/doi/pdf/10.1145/3155126?download=true).” XRDS: Crossroads 24, no. 2 (December 2017): 44–46.
* Daniel Shiffman, 10.1: Introduction to Data and APIs in Javascript, The Coding Train, [online] Available at: https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r [Accessed 13 Mar. 2019].  


**In-class structure:**
  - Group 7 presentation: JSON & Text Parsing ...
  - Decoding exercise on *Vocable Code* by Winnie Soon
  - Textuality
  - Type
  - JSON
  - Exercise in class
  - Rule-based system vs Randomness vs [Dada Poem Generator](http://www1.lasalle.edu/~blum/c340wks/DadaPoem.htm) with cut up technique
  - Showcase other projects
  - Mini-ex walk-through: An electronic literature (group work - 2 to 3 people only) | due SUN mid-night
---

#### Class 10, 0900-13.00 | Week 14 | 31 Mar: Que(e)ry Data
##### With Wed tutorial session only (set up your own study group on Fri)

**Required Reading:**
* Snodgrass, Eric, & Winnie Soon. "[API practices and paradigms: Exploring the protocological parameters of APIs as key facilitators of sociotechnical forms of exchange](https://firstmonday.org/ojs/index.php/fm/article/view/9553/7721)." *First Monday* [Online], 24.2 (2019): n. pag. Web. 13 Jan. 2020
* Daniel Shiffman, Working with data, The Coding Train, available at https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r (**watch 10.4-10.10**)

**Suggested Reading:**
* Kirschenbaum, Matthew G. *Mechanisms: New Media and the Forensic Imagination*, MIT Press, 2007, 25-71.
* Raetzsch, Christoph, et al. “[Weaving Seams with Data: Conceptualizing City APIs as Elements of Infrastructures.](https://journals.sagepub.com/doi/full/10.1177/2053951719827619)” *Big Data & Society*, Jan. 2019, doi:10.1177/2053951719827619.
* Albright, Jonathan. "[The Graph API: Key Points in the Facebook and Cambridge Analytica Debacle](https://medium.com/tow-center/the-graph-api-key-points-in-the-facebook-and-cambridge-analytica-debacle-b69fe692d747)". Medium, 2018. (check out the recent hot topic around Cambridge Analytica online)
* Bucher, Taina, “Objects of intense feeling: The case of the Twitter API”, *Computational Culture: a journal of software studies*. 2013. Web. 27 Nov. 2013. http://computationalculture.net/article/objects-of-intense-feeling-the-case-of-the-twitter-api

**In-class structure:**
  - Group 8 presentation: Image Data Visualization ...
  - Project showcase: *Net Art Generator* by Cornelia Sollfrank, updated by Winnie Soon
  - Image processing: fetching, loading and display
  - Application Programming Interfaces
  - Exercise in class
  - Mini-ex walk-through: Working with APIs (group work) | due SUN mid-night
---

#### EASTER BREAK - NO CLASS
---

#### Class 11, 0900-13.00 | Week 16 | 14 Apr: Algorithmic Procedures
##### With Wed tutorial session only (set up your own study group on Fri)

**Required Reading**
* Ensmenger, Nathan. "The Multiple Meanings of a Flowchart." *Information & Culture: A Journal of History*, vol. 51 no. 3, 2016, pp. 321-351. Project MUSE, doi:10.1353/lac.2016.0013 (check AU e-library)
* Bucher, Taina. *If...THEN: Algorithmic Power and Politics*, Oxford University Press, 2018, pp. 19-40 (The chapter called "The Multiplicity of Algorithms") (check blackboard or our AP library shelf)
* [18 mins video:]Marcus du Sautoy, Algorithms - The Secret Rules of Modern Living - BBC documentary https://www.youtube.com/watch?v=k2AqGongii0). [full ver - optional: https://www.youtube.com/watch?time_continue=2&v=Q9HjeFD62Uk]
* [4 mins video: Algorithms in pseudocode and flow diagrams](https://www.youtube.com/watch?v=XDWw4Ltfy5w)

**Suggested Reading**
* Ed Finn, “What is an Algorithm,” in What Algorithms Want, MIT Press, 2017, pp. 15-56.
* Daniel Shiffman, Multiple js Files - p5.js Tutorial, The Coding Training. Available at: https://www.youtube.com/watch?v=Yk18ZKvXBj4 
* Goffey, Andrew. "Algorithm."*Software Studies\ a lexicon*. Eds. Matthew Fuller. MIT Press, 2008. pp. 15-20.

**In-class structure:**
  - Guest lecture by [Adam Franc](https://is.muni.cz/osoba/416271?lang=en#publikace) (Topic: software: expressive strategies
of software art)
  - Group 9 presentation: Sorting problem ...
  - Discussion on Algorithms
  - Exercise in class
  - Flow chart 
  - Errors
  - Project showcase 
  - Final Project Discussion
  - Mini-ex walk-through: Working together with a Flow Chart (individual and group work) | due SUN mid-night
---

#### Class 12, 0900-13.00 | Week 17 | 21 Apr: Advanced Topic
##### With Wed tutorial session only (set up your own study group on Fri)

** PREPARE and RESEARCH around the topic Machine Learning / Automation.

**Required Reading**
* [A short text] Geoff Cox. [Machine ways of seeing](https://unthinking.photography/themes/machine-vision/ways-of-machine-seeing). *Unthinking Photography*, 2016.
* [ml5js](https://ml5js.org/)

**Suggested Reading**
* Edobson, James. E. ["Can An Algorithm Be Disturbed?: Machine Learning, Intrinsic Criticism, and the Digital Humanities."](https://pdfs.semanticscholar.org/987a/c2ba1da176d52036023f7ef05f47c6366d29.pdf) College Literature, vol. 42 no. 4, 2015, pp. 543-564. Project MUSE

**In-Class structure:**
  - Group 10 presentation: Select a theme ...
  - What is Machine Learning?
  - Teachable Machine
  - Introducing ML js library
  - Project showcase: ELIZA
  - Discussion on students' project
  - Final Project Discussion
  - Mini-ex walk-through: Draft final project (group work) | due SUN mid-night
---

#### Class 13 | Week 18 | 28 Apr - 29 Apr (TUE - WED): Supervision
##### No Wed tutorial session but will turn into supervision across 28-29 Apr

| Time         | Venue  | Group                             
| ------------ |--------|:-------------------------  
| 28.Apr (Tue): 08.30-09.10 | 5361-144 | Group 3-4            
| 28.Apr (Tue): 09.15-10.00 | 5361-144 | Group 5-6                                 
| 28.Apr (Tue): 10.10-10.50 | 5361-144 | Group 7-8 
| 29.Apr (Wed): 14.00-14.40 | 5524-147 | Group 9-10
| 29.Apr (Wed): 14.15-15.55 | 5524-147 | Group 1-2
---

#### no class on Week 19 -  5 May

Your working week for the Final Project 

---
#### Class 14 | Week 20 | 12 May : final presentation + Exam prep + final evaluation 

** LONG DAY **

| Time        | Group                             
| ------------|:------------------------------------------  
| 08.30-08.45 | Group 10            
| 08.50-09.05 | Group 9                                 
| 09.10-09.25 | Group 8                              
| BREAK                             
| 09.35-09.50 | Group 7                              
| 09.55-10.10 | Group 6     
| 10.15-10.30 | Group 5     
| BREAK
| 10.40-10.55 | Group 4                             
| 10.55-11.10 | Group 3     
| 11.15-11.30 | Group 2
| 11.35-11.50 | Group 1
| (LUNCH) BREAK
| 12.15-13.45 | ALL


---
<br>

## SKETCH INSPIRATION:
- [Daily sketch in Processing](https://twitter.com/sasj_nl) by Saskia Freeke, her talk is [here: Making Everyday - GROW 2018](https://www.youtube.com/watch?v=nBtGpEZ0-EQ&fbclid=IwAR119xLXt4nNiqpimIMWBlFHz9gJNdJyUgNwreRhIbdJMPPVx6tq7krd0ww)
- [zach lieberman](https://twitter.com/zachlieberman)
- [Creative Coding with Processing and P5.JS](https://www.facebook.com/groups/creativecodingp5/)
- [OpenProcessing](https://www.openprocessing.org/) (search with keywords)
- [JS1k](https://js1k.com/2018-coins/demos)

## OTHER REFERENCES:
- [JavaScript basics](https://github.com/processing/p5.js/wiki/JavaScript-basics) by p5.js
- [Text and source code: Coding Projects with p5.js by Catherine Leung](https://cathyatseneca.gitbooks.io/coding-projects-with-p5-js/)
- [Video: Crockford on Javascript (more historical context)](https://www.youtube.com/watch?v=JxAXlJEmNMg&list=PLK2r_jmNshM9o-62zTR2toxyRlzrBsSL2)
- [A simple introduction to HTML and CSS](https://sites.google.com/sas.edu.sg/code/learn-to-code)
- McCarthy, L, Reas, C & Fry, B. *Getting Started with p5.js: Making interactive graphics in Javascript and Processing (Make)*, Maker Media, Inc, 2015.
- Shiffman, D. *Learning Processing: A Beginner’s Guide to Programming Images, Animations, and Interaction*, Morgan Kaufmann 2015 (Second Edition)
- Haverbeke,M. *[Eloquent JavaScript](https://eloquentjavascript.net/)*, 2018.
- [Video: Foundations of Programming in Javascript - p5.js Tutorial by Daniel Shiffman](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA)

## Re-examination

Submit all the required 10 mini exercises and a final project but these have to be all done individually. Deadline for submission will be on 31 July 2020.
