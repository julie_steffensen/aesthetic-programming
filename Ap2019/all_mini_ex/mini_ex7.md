# Weekly mini ex7: due week 13, Monday night | A generative program

**Objective:**
- To implement a rule-based generative program from scratch.
- To strengthen the computational use of loops and conditional statements in a program.
- To reflect upon the concept of generativity conceptually and practically, such as systems, rules, temporality, autonomy/authorship, liveness, emergence and processes.

**Get some additional inspiration here:**
- [Generative Design - sketches](http://www.generative-gestaltung.de/2/), [soure code](https://github.com/generative-design/Code-Package-p5.js)
- [GenArt (with source code)](https://github.com/JosephFiola/GenArt) by JosephFiola, see ch.1-4
- [Open Processing with the search keyword 'generative'](https://www.openprocessing.org/browse/?q=generative&time=anytime&type=all#)
- [John Conway's Game of Life](http://web.stanford.edu/~cdebs/GameOfLife/)

**Tasks (RUNME and README):**

**** Optional group work with max 3 people (prefer 2). You can share the runme but the readme must be individual.

1. Make sure you have read/watch the required readings/instructional videos and references on automatism.
2. Start with a blank paper. Think of at least three simple rules that you want to implement in a generative program. (You may take reference from Langton's ant (1986) and The Game of Life (1970))
3. Based on the rules in step 2, then design a generative program that utilizes at least one for-loop/while-loop and one conditional statement but without any direct interactivity. Just let the program runs and emerges automatically.(You may also consider to use noise() and random() syntax)
4. Upload your 'runme' to your own Gitlab account under a folder called **mini_ex7**. (Make sure your program can be run on a web browser)
5. Create a readme file (README.md) and upload to the same mini_ex7 directory (see [this](https://www.markdownguide.org/cheat-sheet) for editing the README). The readme file should contain the followings:
  - A screenshot/animated gif/video about your program (search for how to upload your chosen media and link to your gitlab)
  - A URL link to your program and run on a browser, see: https://www.staticaly.com/ or https://raw.githack.com/.
  - Try to contextualize your sketch by answering these:
    - What are the rules in your generative program and describe how your program performs over time. What have been generated beyond just the end product?
    - What's the role of rules and processes in your work?
    - What's generativity and automatism? How does this mini-exericse help you to understand what might be generativity and automatism? (see the above - objective 3 and the assigned readings)
6. Provide peer-feedback to 2 of your classmates on their works by creating "issues" on his/her gitlab corresponding repository. Write with the issue title "Feedback on mini_ex(?) by (YOUR FULL NAME)" (Feedback is due before next Wed tutorial class while the readme and runme are due on next Mon)

NB1: Feel Free to explore and experiment more syntax and computational structures.

NB2: The readme file should be within 5600 characters.

**mini exercise peer-feedback: guideline**
1. (Just think, no need to write) Think about what kind of feedback you think would be useful to others. What kind of feedback you want to receive by yourself?
2. Describe (basic) what is the program about and how does it works as a (semi-)autonomous system?
3. Do you like the design of the program, and why? and which aspect do you like the most?
4. How about the conceptual linkage on 'generativity' beyond technical description and implementation? What's your thoughts/response on this?
