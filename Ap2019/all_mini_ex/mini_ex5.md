# Weekly mini ex5: due week 11, Monday night | Revisit the past

**Objective:**
- To catch up and explore further with the previous learnt functions and syntaxes
- To revisit the past and design iteratively based on the feedback and reflection on the subject matter.
- To reflect upon the methodological approach of "Aesthetic Programming"

**Tasks (RUNME and README):**
1. Revisit one of the earlier mini exercises that you have done, and try to rework on your design, both technically and conceptually.
2. Upload your 'runme' to your own Gitlab account under a folder called **mini_ex5**. (Make sure your program can be run on a web browser)
3. Create a readme file (README.md) and upload to the same mini_ex5 directory (see [this](https://www.markdownguide.org/cheat-sheet) for editing the README). The readme file should contain the followings:
  - A screenshot/animated gif/video about your program (search for how to upload your chosen media and link to your gitlab)
  - A URL link to your program and run on a browser, see: https://www.staticaly.com/ or https://raw.githack.com/.
  - Try to contextualize your sketch by answering these:
    - what is the concept of your work?
    - what is the departure point?
    - what do you want to express?
    - what have you changed and why?
  - **Based** on the readings that you have done before (see below), What does it mean by programming as a practice? To what extend do you agree the concepts that have been illustrate in below readings? (Can you draw and link some of the concepts in the text and expand with your critical take?) What is the relation between programming and digital culture?
    - Aesthetics Programming: http://darc.au.dk/research/research-agenda/aesthetic-programming/
    - Bergstrom, I. and Blackwell, A.F. (2016). The Practices of Programming. In Proceedings of IEEE Visual Languages and Human-Centric Computing (VL/HCC) 2016.
    - Montfort, Nick. Exploratory Programming For the Arts and Humanities. MIT Press, 2016. 267-277 (Appendix A: Why Program?)
    - Vee, Annette. Coding Literacty: How Computer Programming Is Changing Writing. MIT Press, 2017.43-93 (Ch.1 Coding for Everyone and the Legacy of Mass Literacy)
4. No Peer Feedback this week. But you are required to look at two other classmates' mini_ex5.

NB1: Feel Free to explore and experiment more syntax and computational structures.

NB2: The readme file should be within 8400 characters.
