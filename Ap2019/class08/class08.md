#### Class 08 | Week 13 | 26 Mar 2019: Vocable Code
##### With Wed tutorial session and No Fri shutup and code session (set up your own study group)

### Messy notes:

#### Agenda:

1. Project showcase: Vocable Code by Winnie Soon + contexualization
2. Peer-tutoring: Group 6 / Respondents: Group 7, Topic: JSON
3. Textuality and JSON text files
4. Queering voices
5. Walkthrough next mini-ex8: Generate an electronic literature (group work: 2 to 3 only)
---
#### 1. Project showcase: Vocable Code by Winnie Soon + contexualization
- [RUNME](https://siusoon.github.io/VocableCode/vocablecode_program/)
- [Source code](https://github.com/siusoon/VocableCode/blob/master/vocablecode_program/vocableCode.js)
- [Source code with comment](https://github.com/siusoon/VocableCode/blob/master/vocablecode_program/vocableCode_xx.js)
- [JSON file](https://github.com/siusoon/VocableCode/blob/master/vocablecode_program/inclusive/voices.json)

![installation](https://farm2.staticflickr.com/1772/42269344360_a1038a1471_z.jpg)

<blockquote>
Vocable Code is both a work of “software art” (software as artwork, not software to make an artwork) and a “codework” (where the source code and critical writing operate together) produced to embody “queer code”. It examines the notion of queerness in computer coding. Through collecting voices and statements from others that help to complete the sentence that begins: “Queer is…”, the work is computationally and poetically composed where the texts and voices are repeated and disrupted by mathematical chaos, creating a dynamic audio-visual literature and exploring the performativity of code, subjectivity and language. Behind the executed web interface, the code itself is deliberately written as a codework, a mix of a computer programming language and human language, exploring the material and linguistic tensions of writing and reading within the context of (non)binary poetry and computer code.
</blockquote>


**How does the piece work?**

Can you describe the different elements of the work and imagine how they operate computationally in human language?


**Thinking around code and language:**
- human language and programming language
- abstraction and translation
- discrepancy of executed / source code
- semiotics (signs and symbols)
  - semantics: the relations of signs to their context
  - syntactics: formal structure
- reading/writing/speaking
- Expressively: different voices
- Subjectivity
- Bodily meaning -> social bodies -> meaning making 
- Performativity
- code and poetry
- electronic literature

#### 3.Textuality: typography, text size, alignment and style

**New syntax:**

- call back vs preload

```javascript
function SpeakingCode(iam, makingStatements) {
	let getVoice = "inclusive/voices/" + iam + makingStatements + ".wav";
	speak = loadSound(getVoice, speakingNow);
}
```
- Textuality

```javascript
let withPride; //font
//new font: line 11
function preload() {
  withPride = loadFont('inclusive/Gilbert_TypeWithPride.otf');
}
.
.
.
//line 68: the class and constructor
function notNew(getQueer){
  this.size = floor(random(15.34387,30.34387));
  this.xxxxx = width/2.0;
  this.yyyyy = random(height/3.0,height+20.0);
  this.speed = random(2.34387,3.34387);
  this.gradient = 240.0;
}
.
.
.
this.shows = function() {
//font, size, alignment, fill and position: line 80
  textFont(withPride);
  textSize(this.size);
  textAlign(CENTER);
  noStroke();
  fill(this.gradient);
  text(getQueer, this.xxxxx, this.yyyyy);
}
```

- conditional structure (AND case and OR case)

```javascript
//line 63
if ((queerRights.length <= 2.0) && (frameCount % 20 == 4.0)) {
		makeVisible();
}
```

```javascript
//line 90
this.isInvisible = function() {
		var status;
		if (this.yyyyy <= 4.34387 || this.yyyyy >= height+10.34387) {
			status = "notFalse";
		} else {
			status = "notTrue";
		}
		return status;
};
```

- reading JSON and speaking code

```javascript
let whatisQueer;

function preload() {
  whatisQueer = loadJSON('inclusive/voices.json');
}
.
.
.
function makeVisible() {
//line 16 (queers is the array)
  queers = whatisQueer.queers;
//line 33 (check the JSON file - which are the objects under 'queers' -> to select which voice to play)
  SpeakingCode(queers[WhoIsQueer].iam, makingStatements);
}
```

![sketch](ch7_4.png)

```javascript
function makeVisible() {
  ...
  SpeakingCode(queers[WhoIsQueer].iam, makingStatements);
}
.
.
.
function SpeakingCode(iam, makingStatements) {
	let getVoice = "inclusive/voices/" + iam + makingStatements + ".wav";
	speak = loadSound(getVoice, speakingNow);
}

function speakingNow() {
	speak.play();
}

```

##### Discussion:

<blockquote>
p. 24: "Wall was emphasizing the point that code has expressive qualities, and the need for programmers to 'express both their emotional and technical natures simultaneously"
</blockquote>

<blockquote>
p. 25: "The interplay of the body of the code, the programmer's comments, and the human-machine reader express how hardware and software, text and code, are embodied"
</blockquote>
Cox 2013

Go through the notes that you have taken, then go through the runme and the source code of Vocable Code.

- What are the speech-like qualities of voices? (Don't just take literally about the participants' voice)
- Can you draw a relation between the two pieces (Vocable Code in both artwork and text) based on the notes that you have written?

#### 4. Queering voices

Task: Add your voices + Update the program with your own voices (Work in groups)

**Instruction of voice recording:**

The program takes in wav file format only.
1. Find a blank paper and prepare to write a sentence
2. Complete the sentence with the starting words: “Queer is ... “
  - Each sentence contains no more than 5 words (the starting words- “queer is” is not included)
  - More than 1 sentence is allowed but not more than 2.
  - It is ok to have just one word.
3. Download/Locate a voice recording app on your smartphone (e.g “Voice Recorder” on Android phone or ‘’Voice Memos app” on iphone).
4. Try to find a quiet environment and record your voice, and see if the app works (controlling the start and end of the recording button).
5. Prepare to record your voice with your written sentence(s).
  - It is up to you to decide the temporality and rhythm of speaking the text.
  - It is up to you to either speak the full word or full sentence with different pitch/tempo/rhythm.
  - You can also speak on a certain part (phonetics) of the word or sentence. In other words, the word/sentence doesn’t need to be fully pro-nounced.
  - The first two provided words “queer is” can be omitted.
6. Record your voice, and convert your voice file into wav file format. (audicity is one of software program to do the conversion)
7. Go to next to update the code and incorporate your voice.

**How to update the code**
1. Download the sketch [here](https://github.com/siusoon/VocableCode), click the green download button
2. go to the folder "vocablecode_program" and look for inclusive\voices.json and inclusive\voices
3. Rename your voice file to firstnameLastname2.wav (and if you have more, then rename as firstnameLastname3.wav)
4. Append your info to the json file, fill in the details iam, statement2, and statement3.
5. Run the program

##### Flow chart
p.2 https://dobbeltdagger.net/sites/default/files/vocablecode13082018_stack_winniesoon.pdf

---
#### 5. Walkthrough next mini-ex8 and next week
- See [here](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex) for the mini ex8
- Peer Tutoring next week:  Group 7 / Respondents: Group 8, Topic: Parsing
