#### Class 07 | Week 12 | 19 Mar 2019: Automatisms
##### With Wed tutorial session and Fri shutup and code session.

### Messy notes:

#### Agenda:
1. Automatism
2. Peer-tutoring- Topic: Noise + Randomness + Generative Art
3. Langton Ant: Project + Sample Code + Rule-based and emergent systems
4. Final Project Brief
5. Mid way evaluation
6. Walkthrough next mini-ex7: A generative program
---

#### 2. Automatism

- [The recode project](http://recodeproject.com/)

- the idea of 'rule-based system' can be dated back to the 60s in conceptual art e.g Sol LeWitt
![sol](https://image.slidesharecdn.com/massmocaunitsollewitt-170417221834/95/mass-moca-sol-lewitt-30-638.jpg)

- [Exhibition: Programmed: Rules, Codes, and Choreographies in Art, 1965–2018](https://whitney.org/exhibitions/programmed) in the Whitney Museum of American Art

**Rule, Instruction, Algorithm:
Ideas as Form**
<blockquote>
Artists have long used instructions and abstract concepts to produce their work, employing mathematical principles, creating thought diagrams, or establishing rules for variations of color. Conceptual art—a movement that began in the late 1960s—went a step further, explicitly emphasizing the idea as the driving force behind the form of the work. In his “Paragraphs on Conceptual Art” (1967), Sol LeWitt wrote: “The plan would design the work. Some plans would require millions of variations, and some a limited number, but both are finite. Other plans imply infinity.” The works in this grouping—from Sol LeWitt’s large-scale wall drawing and Josef Albers’s series of nesting colored squares and rectangles to Lucinda Childs’s dances and Joan Truckenbrod’s computer drawings—all directly address the rules and instructions used in their creation. Essential to each is an underlying system that allows the artist to generate variable images and objects.
</blockquote>

**Rule, Instruction, Algorithm:
Generative Measures**
<blockquote>
“Generative art” is defined as any art practice in which the artist hands over control to a system that can function autonomously and that contributes to or creates a work of art. These systems range from natural language instructions and mathematical operations to computer programs and biological processes. While artworks with generative qualities appear throughout the exhibition, works by Ian Cheng, Alex Dodge, and Cheyney Thompson underscore their own process of coming into being or emergence. This emphasis allows us to see an artwork as an open process, where algorithms enable variations in form. Whether using code or chat bots—computer programs designed to simulate conversation with human users—each of these works invites us to rethink authorship, materiality, communication, and meaning.
</blockquote>

- 10 print
![10print](https://raw.githubusercontent.com/AUAP/AP2018/master/class06/10Print.png)

  - the related 10 print [experimentation](https://twitter.com/search?q=%2310print&src=typd)

#### 3. Langton Ant: Project + Sample Code + Rule-based and emergent systems

![langton](langtonAnt.gif)

- Example: [RUNME](https://glcdn.githack.com/siusoon/aesthetic-programming/raw/master/Ap2019/class07/sketch07/index.html) with 3 modes of behaviors
  - Simplicity -> creating a simple pattern
  - Chaos -> It forms a complex pattern
  - Emergent order -> start building a recurrent 'highway' pattern that repeats indefinitely

![ant](http://datagenetics.com/blog/september22015/anim.gif)

- five cell neighborhood (N, E, S, W) + specific behaviors (rules) + start with an initial state

![cell](cell.png)

```javascript
//The rules
/*
1. draw a grid with your desired size (grid_space) via initializing a grid structure based on width and height, columns and width
2. set all the cell states as 0 (where 0 =off/yellow, 1 = on/black)
3. set initial (current) x, y position and direction in the setup()
4. logic starts - in the draw():
    - based on the current position to check if the cell hits the edges (width and height)
    - check current status of the cell against the rules (2 rules in total and that also defines the on/off state)
      Rule1:  At the current cell with the status off/0 square, turn 90° right, flip the color to on/1, move forward one unit
      Rule2:  At the current cell with the status on/1 square, turn 90° left, flip the color to off/0, move forward one unit
    - change color of the cell
    - update corresponding/latest ant's direction + state
    - move to next cell and loop again
*/
```

- [Langton's Ant](https://en.wikipedia.org/wiki/Langton%27s_ant)

- "The possibility that life could emerge from the interaction of inanimate artificial molecules." - Langton 1986

- "how the collections of inanimate molecules that constitute living organisms interact with each other to maintain and perpetuate the living state" - Langton 1986

- a reference "to Turing machines and can thus perform any computable task"

- With basic computing elements, the cells, are only locally connected: "simulating the molecular logic of life"

- "constitute discrete space/time dynamical systems" - Langton 1986, p. 124

- "phase space" => the space defined by all of its variables Langton 1986, p. 124

- chaotic system -> "give the appearance of being random and unpredictable"..."even when the rules governing their behavior are completely deterministic" - Langton 1986, p. 124

- "These simple, simulated ants, whose behavior is dictated by an extremely simple set of rules can, when they act collectively, exhibit some of the phenomena exhibited by large societies of much more complex organisms" - Langton 1986, p. 136

- See [Time of Doubles (2012)](https://vimeo.com/38609719) by Haru Ji and Graham Wakefield

- See [automata I](https://isohale.com/Development-1) by Catherine Griffiths


```javascript
// on 2D array
function setup() {
  grid = drawGrid();
}

function drawGrid() {
  cols = width/grid_space;
  rows = height/grid_space;
  let arr = new Array(cols);
  for (let i=0; i < cols; i++) {//no of cols
    arr[i] = new Array(rows); //2D array
    for (let j=0; j < rows; j++){ //no of rows
      let x = i * grid_space; //actual x coordinate
      let y = j * grid_space; //actual y coordinate
      stroke(50);
      strokeWeight(0.3);
      noFill();
      rect(x, y, grid_space, grid_space);
      arr[i][j] = 0;  // assign each cell with the status 0 / off
    }
  }
  return arr; //a function with a return value
}
```

#### Discussion:
- What have been generated beyond the end product but to focus on systems and processes? or How would you understand "autonomy is the ultimate goal" (Marius Watz 2007)?
- What is the role of rules and "software as material" (Marius Watz 2007), and do you consider the machine as a co-author?

---
#### 5. Final Project Brief
- See [here](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex)

---
#### 6. Mid-way evaluation

- The concept of low threshold and high ceiling
- coding, writing and critical thinking

![ap](ap.png)

[Discuss in groups](https://padlet.com/wsoon/aestheticprogramming):
- What have you learnt in Aesthetic Programming?
- What is the most remarkable example/theme/ex/thing?
- How do you think about the course in relation to:
  coding environments, peer-tutoring, weekly mini ex, notes/slides/sample code, assigned/suggested readings, Tue/Wed/Fri sessions, guest lectures/processing community day/symposium etc?
- What are the things you can do to make the course better?
- What could be changed/experimented in the course to make it better?
- How can we better use our peers?

---
#### 7. Walkthrough next mini-ex7 and next week
- See [here](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex) for the mini ex7
- Peer Tutoring next week
  - Group 6 / Respondents: Group 7, Topic: JSON
