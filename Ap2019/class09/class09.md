#### Class 09 | Week 14 | 2 Apr 2019: Que(e)ries and APIs
##### With Wed tutorial session and No Fri shutup and code session (set up your own study group)

### Messy notes:

#### Agenda:

1. Project showcase: Net Art Generator by Cornelia Sollfrank + contexualization on APIs
2. Peer-tutoring: Group 7 / Respondents: Group 8, Topic: Parsing
3. Image processing: fetching, loading and display
4. Walkthrough Google API together
5. Different types of errors
6. Walkthrough next mini-ex9: Working with APIs (group work)
---

#### 1. NET ART GENERATOR by Cornelia Sollfrank, ver5b updated by Winnie Soon
[Artwork](http://nag.iap.de/)

<img src="nag.png" width="450">

<img src="netartgenerator.png" width="450">

<img src="api.png" width="450">

<img src ="api1.png" width="450">

<img src="api2.png" width="450">


<blockquote>
APIs typically do so with a view to facilitating forms of exchange and interoperability between these components and the corresponding applications and/or larger systems within which they are situated.
</blockquote>

<blockquote>
 an API provides methods of controlled access to computational components, creating a standardized grammar of functionality in order to facilitate forms of exchange between various components and agents in a manner that typically helps to make them interoperable and independent of their respective implementations.
</blockquote>

<blockquote>
What we want to highlight here is that these discussions are not simply of a technical nature. Rather, these technical questions and their often shifting specifications and parameters of possibility point to how processes of control and exchange are enacted through the execution of technical parameters with both immediate and longer term effects.
</blockquote>

#### 3. Image processing: fetching, loading and display
- `loadJSON()`: use GOOGLE API to fetch search images with a specific keyword. This is a call back function as it takes time to fetch the file.
- `loadImage()` and `image()` are used to display the image4
- `loadPixels()`: Loads the pixel data into the pixels[] array  (https://p5js.org/reference/#/p5/loadPixels)
- `line()` is used to visualize the particular color of the image's pixel.

#### 4. Google API walkthrough- in-class ex
![image](flowers.gif)
- **Basic: The sample code is about getting a static image from Google image search API (via parsing JSON), and then display it on a screen, you need to get the key ID and Engine ID so that the program can run and fetch a networked image on the fly**. This requires you to:
1. Understand the Google image search API workflow
2. Understand the API specification
3. Understand the corresponding returned JSON file format from Google image search API
4. Able to get the API key and search engine ID from Google
5. Configure the settings at the Google image search console
- Advanced: Uncomment line 48-55, where you will get the effects in which the image color of the image will be picked up and presented as animated lines.
```javascript
img.loadPixels();
img_x = floor(random(0,img.width));
img_y = floor(random(0,img.height));
loc = (img_x+img_y * img.width)*4; // formular to locate the no: x+y*width, indicating which pixel of the image in a grid (and each pixel array holds red, green, blue and alpha values) can see more here: https://www.youtube.com/watch?v=nMUMZ5YRxHI
stroke(color(img.pixels[loc],img.pixels[loc + 1], img.pixels[loc+2]));
line(img_x,0,img_x,height);
```

#### Step by Step:
1. [Basic] Create a p5 sketch, then copy and paste the [source code](xxx) to your code editor. (you need to have a html file and the p5 library.
2. [Basic] The objective is to replace the API KEY with your own on line 8
```javascript
var apikey = "INPUT YOUR OWN KEY";
```
![image4](image4.png)
- Go to [Custom Search JSON/Atom API](https://developers.google.com/custom-search/json-api/v1/overview)
- click the blue botton “Get A Key” -> Create project -> Enter your project name (e.g miniEx9) -> Click 'Create and Enable API’ -> Copy your API key and paste on your program > line 7.
3. [Basic] After you get the API key, next is to replace the search engine ID (cx) on line 9
```javascript
var engineID = "INPUT YOUR OWN";
```
![image5](image5.png)

- Get the search engine ID (CX) [here](https://cse.google.com/all)
- Add search engine -> Put something in “Sites to search” (e.g http://www.google.com) and “Name of the search engine” (e.g miniEx9) -> Click ‘create’; “Details” with ‘Search Engine ID’
- Copy the ID (that is the cx) and paste on your program > line 9.

4. [Basic] Settings on the control panel:
- select “search the entire web but emphasize included sites” 
- Make sure the “Image Search” is ON > click **update**

5. [Basic] Try to run your program and see if any image display on your screen (turn your browser console on)

6. [Basic] How to get the exact data?

```javascript
function fetchImage() {
	request = url + "key=" + apikey + "&cx=" + engineID + "&imgSize=" + imgSize + "&q=" + query;
	console.log(request);
	loadJSON(request, gotData); //this is the key syntax and line of code to make a query request and get a query response
}

function gotData(data) {   //a callback needs an argument
	getImg = data.items[0].pagemap.cse_thumbnail[0].src;  // this is the thumbnail
}
```
![image6](image6.png)

7. [Basic] What if I want to search for other keywords or other image format?
- Change your query on line 11, right now the keywords is warhol flowers. The program won't take space between text, please use '+' sign to join the text.
```javascript
var query = "warhol+flowers"; //search keywords
```
- look at [here](https://developers.google.com/custom-search/json-api/v1/reference/cse/list#parameters) to change different parameters, such as adding imgColorType etc.
- The whole URL is as below, and there is a total of 100 free requests and each parameter is seperated by a '&' sign
https://www.googleapis.com/customsearch/v1?key=YOURKEY&cx=YOURID&imgSize=small&q=warhol+flowers

8. [Advanced] If you want to work with the advanced code, you need:
- uncomment line 35-42
- if you are using Google chrome, please install an extra add on and then restart the browser and turn it on: [Allow-Control-Allow-Original:*](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en) (it is because the image is hosted with different domain, the current browser does not allow you to manipulate an image in this way. You can either install this add on or do server side programming like using node.js)
- It seems working fine on Firefox (at least for my browser), perhaps you may need to restart the browser
- You should able to load the image and with the animated effects, but still you should encounter 'Type Error' on your browser console. It is because The image retrieval is not put in the preload function and it is generated on the fly, and it takes time to load the image and get the pixel color value in function draw(). (i.e once a program starts, there is no image to 'loadpixel' in draw() and it takes time for draw() to pick up the fetched image from Google.) I have used the '[try and catch statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/try...catch)' to keep the code running over time (it is a javascript syntax). If you want to get rid of the error, comment out the line 45- console.error(error);

```javascript
try {	//takes time to load the external image, that's why you see errors in the console.log
  loadImage(getImg, function(img) {
  push();
  translate(width/2-img.width/2, 0);
  image(img,0,0);
   //try to uncomment this block if you manage to get the image.
  img.loadPixels();
  img_x = floor(random(0,img.width));
  img_y = floor(random(0,img.height));
  loc = (img_x+img_y * img.width)*4; // formular to locate the no: x+y*width, indicating which pixel of the image in a grid (and each pixel array holds red, green, blue and alpha values - 4) can see more here: https://www.youtube.com/watch?v=nMUMZ5YRxHI
  stroke(color(img.pixels[loc],img.pixels[loc + 1], img.pixels[loc+2]));  //rgb values
  line(img_x,0,img_x,height);
  pop();
  });
}catch(error) {
    console.error(error);
}
```

#### 5. Different types of errors
At this stage, you have better programming skills and your program will be more complex. You need to develop a way to understand, identify and locate errors. Is the error from your own writing of the program, or error from parsing the data, or error from Google? Are they minor errors or critical errors (that stop your program from running)? Are they belong to syntactic, runtime or logical errors? (see below) For example, if you encounter error 403 in your console, this likely means that Google has barred your API as the requests exceed the 100 times.

In a broad sense, errors can be categorized in three types:

A. **Syntax errors**: problems with the syntax, also known as parsing errors. -> easier to catch and can be detected by a parser (i.e the browser in this case) e.g spelling errors or missing a closed bracket
```javascript
SyntaxError: missing ) after argument list
```
B. **Runtime errors**: It happens during the execution of a program and it can cause a program to terminate unexpectedly if an exception is not thrown while the syntax is correct. That is why it is also called exceptions. (e.g TypeError or ReferenceError in Firefox browser)
```javascript
TypeError: Cannot read property 'indexOf' of undefined
    at e.loadImage (p5.min.js:10)
    at draw (sketch09.js:43)
    at e.d.redraw (p5.min.js:9)
    at e.<anonymous> (p5.min.js:8)
```  
  - Use [Try/Catch/Finally/Throw an exception](https://www.w3schools.com/js/js_errors.asp)
  
C. **Logical errors**: Arguably the hardest error to locate as it deals with logics but not syntax. The code may still run perfectly but the result is not what you expected.
  - try to identify where errors might occur, down to block of code or line of code
  - try to identify type errors
  - Use [Try/Catch/Finally/Throw an exception](https://www.w3schools.com/js/js_errors.asp)

---
#### 6. Walkthrough next mini-ex9 and next week
- See [here](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex) for the mini ex9
- Peer Tutoring next week
