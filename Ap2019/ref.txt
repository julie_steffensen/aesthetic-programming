generativity, random, function, array, poetry
https://nickm.com/poems/ny2019/111_haiku.html

reflection:
- start with web p5.js (for first class) and then introduce atom in a later stage say class 2
- how to introduce let vs var vs const
- emphasis spatial dimension a bit more for class #2
- better with 4 hours for the time theme
- food vs computation:  check 1968 book your career in computers -> "cooking from a cookbook"
- class03 with 15 mins left -> can take slowly in terms of asking questions
- 2d array instructional video :https://www.youtube.com/watch?v=OTNpiLUSiB4
- abstraction ref: Beatrice contingent computation
- data capture one works well and students like to have physical papers to read and discuss code
